package fr.triozer.mkr.manager;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.mkr.world.ability.Ability;
import fr.triozer.mkr.world.ability.InfiniteRainAbility;
import fr.triozer.mkr.world.ability.InfiniteSunAbility;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */

public class AbilityManager implements AbstractManager<Class<? extends Ability>, String> {

    private final Map<String, Class<? extends Ability>> abilities;

    public AbilityManager() {
        this.abilities = new HashMap<>();

        add(InfiniteRainAbility.class);
        add(InfiniteSunAbility.class);
    }

    @Override
    public Class<? extends Ability> get(String key) {
        return this.abilities.get(key);
    }

    @Override
    public void add(Class<? extends Ability> value) {
        try {
            Bukkit.broadcastMessage("+" + value.getDeclaredMethod("getTag"));
            this.abilities.put(String.valueOf(value.getDeclaredMethod("getTag")), value);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Class<? extends Ability> value) {
        this.abilities.remove(value);
    }

    @Override
    public final Stream<Class<? extends Ability>> values() {
        return this.abilities.values().stream();
    }

    public final Ability random(World world) {
        Object[] objects = this.abilities.values().toArray();
        return toAbility((Class<? extends Ability>) objects[new Random().nextInt(objects.length)], world);
    }

    public final Ability toAbility(Class<? extends Ability> abClass, World world) {
        try {
            return abClass.getConstructor(World.class).newInstance(world);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }


}
