package fr.triozer.mkr.manager;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.mkr.Minekahest;
import fr.triozer.mkr.world.MWorld;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class WorldManager implements AbstractManager<MWorld, String> {

    private final Map<String, MWorld> worlds;

    public WorldManager() {
        this.worlds = new HashMap<>();

        new Task().runTaskTimer(Minekahest.getInstance(), 0L, 20L);
    }

    @Override
    public MWorld get(String key) {
        return this.worlds.get(key);
    }

    @Override
    public void add(MWorld value) {
        Bukkit.broadcastMessage("+" + value.getName());
        this.worlds.put(value.getName(), value);
    }

    @Override
    public void remove(MWorld value) {
        this.worlds.remove(value.getName());
    }

    @Override
    public Stream<MWorld> values() {
        return this.worlds.values().stream();
    }

    class Task extends BukkitRunnable {
        private int seconds;

        @Override
        public void run() {
            Set<MWorld> remove = new HashSet<>();
            seconds++;

            values().forEach(world -> {
                if (world.getLeft() >= 0) {
                    world.update();

                    if (seconds == 10) {
                        world.ttl();
                        seconds = 0;
                    }
                } else {
                    remove.add(world);
                }
            });

            if (!remove.isEmpty()) {
                remove.forEach(world -> {
                    world.destroy();
                    worlds.remove(world.getName());
                });
                remove.clear();
            }
        }

    }

}
