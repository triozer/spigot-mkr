package fr.triozer.mkr.command;

import fr.triozer.api.command.AbstractCommand;
import fr.triozer.api.message.Message;
import fr.triozer.mkr.Minekahest;
import fr.triozer.mkr.world.MWorld;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author Cédric / Triozer
 */
public class PortalCommand extends AbstractCommand {
    public PortalCommand() {
        super(Minekahest.getInstance(), "portal", "minekahest.portal", true, 0);
    }

    @Override
    protected void execute(Player player, String[] args) {
        if (args.length == 0) {
            showHelp(player);
            return;
        }

        MWorld mWorld = new MWorld(player.getLocation());
        mWorld.add(player);

        mWorld.info(player);
    }

    @Override
    protected List<String> tabCompleter(Player player, String[] args) {
        return super.tabCompleter(player, args);
    }

    @Override
    protected List<Message> addHelp(CommandSender sender) {
        return null;
    }
}