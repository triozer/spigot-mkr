package fr.triozer.mkr.world;

import fr.triozer.api.message.ChatMessage;
import fr.triozer.api.message.Message;
import fr.triozer.api.npc.NpcBuilder;
import fr.triozer.api.property.DataProperty;
import fr.triozer.api.scoreboard.IndividualScoreboard;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import fr.triozer.mkr.Minekahest;
import fr.triozer.mkr.world.ability.Ability;
import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.*;
import java.util.function.BiConsumer;

/**
 * @author Cédric / Triozer
 */
public class MWorld {

    private final UUID id;
    private final int SECONDS_IN_A_DAY = 24 * 60 * 60;
    private final Location   portalLocation;
    private final NpcBuilder info;

    private String name;
    private World  world;

    private long   expire;
    private long   left;
    private String expireFormat;

    private List<User>   players;
    private Set<Ability> abilities;

    public MWorld(Location portalLocation) {
        this(portalLocation, UUID.randomUUID().toString().substring(0, 5));
    }

    private MWorld(Location portalLocation, String name) {
        this.id = UUID.randomUUID();
        this.portalLocation = portalLocation;
        this.name = name;

        this.expire = Math.abs(System.currentTimeMillis() + RandomUtils.nextInt(99) * 500L); // 00000
        this.left = expire - System.currentTimeMillis();
        this.players = new ArrayList<>();

        this.start();

        this.abilities = new HashSet<>();
        int i = new Random().nextInt(5) + 1;
        for (int j = 0; j < i; j++) {
            Ability random = Minekahest.getInstance().getAbilities().random(this.world);

            if (containsAbility(random)) continue;
            if (!Collections.disjoint(this.abilities, random.getIncompatibles())) continue;

            this.abilities.add(random);
        }

        this.abilities.forEach(Ability::init);
        this.info = new NpcBuilder(portalLocation, false).name("§aCréation du monde", "§e" + name)
                .addListener(new NpcBuilder.NpcListener() {
                    @Override
                    public BiConsumer<Player, PlayerInteractEntityEvent> click() {
                        return (player, playerInteractEntityEvent) -> add(player);
                    }
                }).build();
        this.calculate();

        Minekahest.getInstance().getWorlds().add(this);
    }

    private boolean containsAbility(Ability ability) {
        return this.abilities.stream().anyMatch(content -> content.getTag().equals(ability.getTag()));
    }

    private void start() {
        Bukkit.createWorld(new WorldCreator(this.name));
        this.world = Bukkit.getWorld(this.name);
    }

    public void add(Player player) {
        User user = TrioCore.getInstance().getPlayers().getUser(player);
        user.getProperty().set("mkr." + this.name + ".point", DataProperty.EMPTY);
        this.players.add(user);

        if (user.getScoreboard() != null) user.getScoreboard().end();

        IndividualScoreboard scoreboard = new IndividualScoreboard(user.getPlayer(), "§e§lMineKahest");
        user.setScoreboard(scoreboard);

        user.getScoreboard().white(0);
        user.getScoreboard().setLine(1, "  §eMonde: §a" + this.name);
        user.getScoreboard().setLine(2, "  §ePoint: §a" + user.getProperty().get("mkr." + this.name + ".point"));
        user.getScoreboard().white(3);
        user.getScoreboard().white(4);
        user.getScoreboard().setLine(5, "  §aLe monde expire dans");
        user.getScoreboard().setLine(6, "         §epatienter");
        user.getScoreboard().white(7);
    }

    private long calculate() {
        this.left = expire - System.currentTimeMillis();
        long diffSec = left / 1000;

        long days       = diffSec / SECONDS_IN_A_DAY;
        long secondsDay = diffSec % SECONDS_IN_A_DAY;
        long seconds    = secondsDay % 60;
        long minutes    = (secondsDay / 60) % 60;
        long hours      = (secondsDay / 3600); // % 24 not needed

        expireFormat = String.format("%dj %dh %dm %ds", days, hours, minutes, seconds);

        return days;
    }

    public void destroy() {
        this.info.end();
        this.players.forEach(user -> user.getScoreboard().end());

        new MWorld(portalLocation);
    }

    public void update() {
        long days = calculate();

        this.info.set(0, "§eMonde: §a" + this.name);
        this.info.set(1, "§eExpire dans: §a" + this.expireFormat);

        this.players.forEach(user -> {
            String space = "     ";
            if (days > 9 && days < 100)
                space += "  ";
            else if (days < 10)
                space += "  ";

            user.getScoreboard().setLine(6, space + "§e" + this.expireFormat);
        });
    }

    public void ttl() {
        this.players.forEach(user -> {
            user.getProperty().add("mkr." + this.name + ".point", new Random().nextInt(4));
            user.getScoreboard().setLine(2, "  §ePoint: §a" + user.getProperty().get("mkr." + this.name + ".point"));
        });
    }

    public final UUID getID() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final World getWorld() {
        return this.world;
    }

    public final long getExpire() {
        return this.expire;
    }

    public final long getLeft() {
        return this.left;
    }

    public final String getExpireFormat() {
        return this.expireFormat;
    }

    public final List<User> getPlayers() {
        return this.players;
    }

    public void info(CommandSender sender) {
        List<Message> messages = new LinkedList<>();

        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage("§7§l----[ §e§lMineKahest §7§l]----"));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage("  §eMonde: §a" + this.name));
        messages.add(new ChatMessage("  §eExpire dans: §a" + this.expireFormat));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage("  §eCaractéristiques du monde:"));
        String[] abilitiesInfo = abilitiesInfo();
        messages.add(new ChatMessage("    " + abilitiesInfo[0]));
        messages.add(new ChatMessage("    " + abilitiesInfo[1]));
        messages.add(new ChatMessage("    " + abilitiesInfo[2]));
        messages.add(ChatMessage.WHITE);
        messages.add(new ChatMessage("§7§l--------[ == ]--------"));
        messages.add(ChatMessage.WHITE);

        messages.forEach(message -> message.send(sender));
    }

    private String[] abilitiesInfo() {
        StringBuilder bonus   = new StringBuilder("§eBonus §7(" + this.abilities.stream().filter(a -> a.getEffect() == Ability.Effect.BONUS).count() + ")§e : ");
        StringBuilder malus   = new StringBuilder("§eMalus §7(" + this.abilities.stream().filter(a -> a.getEffect() == Ability.Effect.MALUS).count() + ")§e : ");
        StringBuilder neutral = new StringBuilder("§eNeutre §7(" + this.abilities.stream().filter(a -> a.getEffect() == Ability.Effect.NEUTRAL).count() + ")§e : ");

        StringJoiner bonusJoiner   = new StringJoiner(", ");
        StringJoiner malusJoiner   = new StringJoiner(", ");
        StringJoiner neutralJoiner = new StringJoiner(", ");
        for (Ability ability : this.abilities) {
            if (ability.getEffect() == Ability.Effect.BONUS) {
                bonusJoiner.add(ability.getName());
            } else if (ability.getEffect() == Ability.Effect.MALUS) {
                malusJoiner.add(ability.getName());
            } else {
                neutralJoiner.add(ability.getName());
            }
        }

        bonus.append(bonusJoiner);
        malus.append(malusJoiner);
        neutral.append(neutralJoiner);

        return new String[]{bonus.toString(), malus.toString(), neutral.toString()};
    }

}
