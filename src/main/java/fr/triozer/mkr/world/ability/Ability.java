package fr.triozer.mkr.world.ability;

import org.bukkit.World;
import org.bukkit.event.Listener;

import java.util.Set;

/**
 * @author Cédric / Triozer
 */
public interface Ability {

    void init();

    String getTag();

    String getName();

    String[] getDescription();

    Set<Class<? extends Ability>> getIncompatibles();

    World getWorld();

    Effect getEffect();

    default boolean isCompatible(Ability ability) {
        return !getIncompatibles().contains(ability);
    }

    Listeners getListeners();

    class Listeners implements Listener {
    }

    enum Effect {

        BONUS, MALUS, NEUTRAL;

    }
}
