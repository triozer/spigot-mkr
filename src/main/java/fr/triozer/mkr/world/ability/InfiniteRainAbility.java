package fr.triozer.mkr.world.ability;

import com.google.common.collect.Sets;
import fr.triozer.api.ui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

/**
 * @author Cédric / Triozer
 */
public class InfiniteRainAbility implements Ability {

    private final World world;

    public InfiniteRainAbility(World world) {
        this.world = world;
    }

    @Override
    public void init() {
        this.world.setStorm(true);
        this.world.setWeatherDuration(-1);
    }

    @Override
    public final String getTag() {
        return "infinite_rain";
    }

    @Override
    public final String getName() {
        return "Pluie infinie";
    }

    @Override
    public final String[] getDescription() {
        return new String[]{"Cet attribut produit une pluie infinie", "sur tout le monde."};
    }

    @Override
    public final Set<Class<? extends Ability>> getIncompatibles() {
        return Sets.newHashSet(InfiniteSunAbility.class);
    }

    @Override
    public final World getWorld() {
        return this.world;
    }

    @Override
    public final Effect getEffect() {
        return Effect.NEUTRAL;
    }

    public final ItemStack getIcon() {
        return new ItemBuilder(Material.WATER_BUCKET)
                .name(getName())
                .lore(getDescription())
                .build();
    }

    @Override
    public Listeners getListeners() {
        return new Listeners() {
            @EventHandler
            public void onWeatherChange(WeatherChangeEvent event) {
                if (!event.toWeatherState()) event.setCancelled(true);
            }
        };
    }

}
