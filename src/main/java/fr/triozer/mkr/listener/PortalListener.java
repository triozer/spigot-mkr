package fr.triozer.mkr.listener;

import fr.triozer.mkr.Minekahest;
import org.bukkit.PortalType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCreatePortalEvent;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;

/**
 * @author Cédric / Triozer
 */
public class PortalListener implements Listener {

    @EventHandler
    public void onCreatePortal(EntityCreatePortalEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (!(event.getPortalType() == PortalType.CUSTOM)) return;

        event.getBlocks().forEach(block -> Minekahest.getInstance().getConsole().fine("Block : " + block.getType() + ", " + block.getLocation()));
    }

    @EventHandler
    public void onEnterPortal(EntityPortalEnterEvent event) {
        if (!(event.getEntity() instanceof Player)) return;

    }

    @EventHandler
    public void onExitPortal(EntityPortalExitEvent event) {
        if (!(event.getEntity() instanceof Player)) return;

    }

}
