package fr.triozer.mkr;

import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.core.TrioCore;
import fr.triozer.mkr.command.PortalCommand;
import fr.triozer.mkr.manager.AbilityManager;
import fr.triozer.mkr.manager.WorldManager;

public final class Minekahest extends TrioPlugin {

    private static Minekahest     instance;
    private        WorldManager   worldManager;
    private        AbilityManager abilities;

    public static Minekahest getInstance() {
        return instance;
    }

    @Override
    protected void enable() {
        instance = this;
        this.worldManager = new WorldManager();
        this.abilities = new AbilityManager();

        // TrioCore.getInstance().getPlayers().values().forEach(user -> new MWorld().add(user));
    }

    @Override
    protected void registerCommands() {
        new PortalCommand().register();
    }

    @Override
    protected void disable() {
        TrioCore.getInstance().getPlayers().values()
                .filter(user -> user.getScoreboard() != null)
                .forEach(user -> user.getScoreboard().end());
    }

    public final WorldManager getWorlds() {
        return this.worldManager;
    }

    public final AbilityManager getAbilities() {
        return this.abilities;
    }

}
